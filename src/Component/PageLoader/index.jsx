import React from 'react';
import Loader from "../../Assest/images/ajax-document-loader.gif";
import "../PageLoader/PageLoader.css";

const PageLoader = () => {
  return (
       <>
         <table className="doc-loader">
         <tbody>
            <tr>
                <td>
                    <img src={Loader} alt="Loading..." />
                </td>
            </tr>
         </tbody>
        </table>
       </>
  )
}

export default PageLoader