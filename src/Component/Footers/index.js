import React from 'react';
import "../../Assest/css/common.css";
import "../../Assest/css/clear.css";
import "../../Assest/css/sm-clean.css";
import "../../Assest/css/style.css";
const Footers = () => {
  return (
      <>
          <footer>
            <div className="footer content-1170 center-relative">
                <ul>
                    <li className="copyright-footer">
                        © 2018 All rights reserved. | Boxus Template by <a href="https://colorlib.com" target="_blank">Colorlib</a>
                    </li>
                    <li className="social-footer">
                        <a href="#"><span className="fa fa-twitter"></span></a>
                        <a href="#"><span className="fa fa-behance"></span></a>
                        <a href="#"><span className="fa fa-dribbble"></span></a>
                        <a  href="#"><span className="fa fa-facebook"></span></a>
                        <a  href="#"><span className="fa fa-rss"></span></a>
                    </li>
                </ul>
            </div>
        </footer>
      </>
  )
}

export default Footers