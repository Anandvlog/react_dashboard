import React from "react";

const InnerListing = ({ title,name,className}) => {

  const user_Tittle = [
    {
      id: 1,
      titleYear: "2001-2003. ",
      titlePara: "Art Studio Lorem Donec",
    },
    {
      id: 2,
      titleYear: "2003-2006. ",
      titlePara: "Per Set Web Site",
    },
    {
      id: 3,
      titleYear: "2006-2010. ",
      titlePara: "Setera Donec EstNunc",
    },
    {
      id: 4,
      titleYear: "2010-2013. ",
      titlePara: "Studio Labore Tempor",
    },
    {
      id: 5,
      titleYear: "2013-2016. ",
      titlePara: "Magna Ipsum Amet",
    },
  ];
  

  const createMarkup =() => {
    return {__html:  title};
  };

  return (
    <>
      <div className="content-wrapper">
        <div className="content-title-holder custom-holder">
          <p className="content-title">{name}</p>
        </div>
        <div className="one_half " dangerouslySetInnerHTML={createMarkup()} />
        <div className="one_half last ">
          {user_Tittle.map((titles,index) => {
            return (
              <React.Fragment key={index}>
                <span className={className} >
                  <strong>{titles.titleYear}</strong>
                </span>
                <span style={{ color: "#727190" }}>
                  <em>{titles.titlePara}</em>
                </span>
                <br />
              </React.Fragment>
            );
          })}
        </div>
        <div className="clear"></div>
        <br />
        <br />
      </div>
    </>
  );
};

export default InnerListing;
