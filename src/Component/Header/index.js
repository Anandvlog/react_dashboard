import React, { useEffect, useState } from "react";
import { Link } from "react-scroll";
import "../../Assest/css/clear.css";
import "../../Assest/css/common.css";
import "../../Assest/css/sm-clean.css";
import "../../Assest/css/style.css";

const Header = () => {
  const [active, setActive] = useState(0);

  const linkData = [
    {
      id: 1,
      key: "home",
      text: "Home",
    },
    {
      id: 2,
      key: "services",
      text: "Service",
    },
    {
      id: 3,
      key: "portfolio",
      text: "Portfolio",
    },
    {
      id: 4,
      key: "about",
      text: "About",
    },
    {
      id: 5,
      key: "news",
      text: "News",
    },
    {
      id: 6,
      key: "video",
      text: "Video",
    },
    {
      id: 7,
      key: "skills",
      text: "Skill",
    },
    {
      id: 8,
      key: "contact",
      text: "Contact",
    },
  ];

  const clickHandler = (name) => {
    setActive(name);
  };
  
  return (
    <>
      <div
        className="menu-wrapper center-relative"
        style={{
          position: "fixed",
          top: "0px",
          backgroundColor:
            active == "services"
              ? "#32db8a"
              : active == "portfolio"
              ? "#ffba42"
              : active == "about"
              ? "#e64b77"
              : active == "news"
              ? "#32db8a"
              : active == "video"
              ? "#ffba42"
              : active == "skills"
              ? "#32db8a"
              : active == "contact"
              ? "#e64b77"
              : "#221c5a",
        }}
      >
        <nav id="header-main-menu">
          <div className="mob-menu">MENU</div>
          <ul className="main-menu sm sm-clean">
            {linkData.map((link, id) => {
            console.log('link', link.key);
              return (
                <li key={id}>
                  <Link
                    to={link.key}
                    id={link.id}
                    delay={200}
                    spy
                    smooth={true}
                    onSetActive={clickHandler}
                  
                  >
                    <span>{link.text}</span>
                  </Link>
                </li>
              );
            })}
          </ul>
        </nav>
      </div>
    </>
  );
};

export default Header;
