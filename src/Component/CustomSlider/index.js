import React from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import iconEdit from "../../Assest/demo-images/icon_editing.png";
import iconClear from "../../Assest/demo-images/icon_clear.png";
import iconDisplay from "../../Assest/demo-images/icon_display.png";
import iconTime from "../../Assest/demo-images/icon_time.png";
import iconIdeas from "../../Assest/demo-images/icon_ideas.png";
import iconSound from "../../Assest/demo-images/icon_sound.png";
import iconSolution from "../../Assest/demo-images/icon_solution.png";

const CustomSlider = () => {
  
  let settings = {
    dots: true,
    arrows: false,
    infinite: true,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 4,
    autoplay: true,
    autoplaySpeed: 6000,

    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          initialSlide: 2,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };

  const slideItems = [
    {
      id: 1,
      title: "Video Editing",
      img: iconEdit,
    },
    {
      id: 2,
      title: "Premium Cleer",
      img: iconClear,
    },
    {
      id: 3,
      title: "For All Displays",
      img: iconDisplay,
    },
    {
      id: 4,
      title: "Right On Time",
      img: iconTime,
    },
    {
      id: 5,
      title: "Innovative Ideas",
      img: iconIdeas,
    },
    {
      id: 6,
      title: "Perfect Sound",
      img: iconSound,
    },
    {
      id: 7,
      title: "3D Solution",
      img: iconSolution,
    },
  ];

  return (
    <>
      <div>

        <Slider {...settings}>
        {slideItems.map((slideItem) => (
            <div key={slideItem.id}>
              <div>
                  <li className="fw-slide" style={{ width: "305px" }}>
                    <img src={slideItem.img} alt="icon_editing" />
                    <p className="fw-slide-text">{slideItem.title}</p>
                  </li>
              </div>
            </div>
          ))}
        </Slider>
      </div>
    </>
  );
};

export default CustomSlider;
