import React from "react";

const CardSection = ({id,name,className}) => {
  return (
    <div className={`section-title-holder ${className}`}>
      <div className="section-num">
        <span>{id}</span>
      </div>
      <h2 className="entry-title">{name}</h2>
    </div>
  );
};

export default CardSection;
