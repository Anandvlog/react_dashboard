import React from "react";
import CardSection from "../../Component/CardSection";

const Skill = () => {
  return (
    <>
      {/* Skills */}
      <div id="skills" className="section">
        <div className="block content-1170 center-relative">
        <CardSection id="06" name="EXPERTISE" className="right"/>
          <div className="section-content-holder left">
            <div className="content-wrapper">
              <div className="content-title-holder custom-holder">
                <p className="content-title">Skills</p>
              </div>
              <p>
                Fusce suscipit, orci eget lobortis sodales, velit nunc tristique
                metus, in tristique odio ante id sem. Etiam in quam et sapien
                bibendum mollis. Morbi eget velit eros, quis imperdiet arcusere
                perdan. Nunc lorem justo, pellentesque ac egestas quis.
              </p>
              <br />
              <div className="progress_bar ">
                <div className="progress_bar_field_holder" style={{width:"81%"}}>
                  <div className="progress_bar_title" style={{color: "#55B286"}}>
                    HTML
                  </div>
                  <div className="progress_bar_percent_text" style={{color: "#55B286"}}>
                    81%
                  </div>
                  <div
                    className="progress_bar_field_perecent"
                    style={{backgroundColor:"#32DB8A"}}
                  ></div>
                </div>
              </div>
              <div className="progress_bar ">
                <div className="progress_bar_field_holder" style={{width:"93%"}}>
                  <div className="progress_bar_title" style={{color: "#E3A536"}}>
                    CSS
                  </div>
                  <div className="progress_bar_percent_text" style={{color:"#E3A536"}}>
                    93%
                  </div>
                  <div
                    className="progress_bar_field_perecent"
                    style={{backgroundColor:"#FFBB42"}}
                  ></div>
                </div>
              </div>
              <div className="progress_bar ">
                <div className="progress_bar_field_holder" style={{width:"72%"}}>
                  <div className="progress_bar_title" style={{color: "#B24564"}}>
                    PSD
                  </div>
                  <div className="progress_bar_percent_text" style={{color: '#B24564'}}>
                    72%
                  </div>
                  <div
                    className="progress_bar_field_perecent"
                    style={{backgroundColor:"#E74C78"}}
                  ></div>
                </div>
              </div>
              <div className="progress_bar ">
                <div className="progress_bar_field_holder" style={{width:"99%"}}>
                  <div className="progress_bar_title" style={{color: "#468ac7"}}>
                    DESIGN
                  </div>
                  <div className="progress_bar_percent_text" style={{color: "#468ac7"}}>
                    99%
                  </div>
                  <div
                    className="progress_bar_field_perecent"
                    style={{backgroundColor:"#4C9EE7"}}
                  ></div>
                </div>
              </div>
              <div className="progress_bar ">
                <div className="progress_bar_field_holder" style={{width:"50%"}}>
                  <div className="progress_bar_title" style={{color: "#468ac79e"}}>
                    REACT JS
                  </div>
                  <div className="progress_bar_percent_text" style={{color: "#468ac79e"}}>
                    50%
                  </div>
                  <div
                    className="progress_bar_field_perecent"
                    style={{backgroundColor:"#221c5a"}}
                  ></div>
                </div>
              </div>
            </div>
          </div>
          <div className="clear"></div>
        </div>
      </div>
    </>
  );
};

export default Skill;
