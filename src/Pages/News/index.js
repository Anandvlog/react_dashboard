import React, { useState } from 'react';
import CardSection from '../../Component/CardSection';



const News = () => {
   const [slideBlog , setSlideBlog] = useState(0); 
   const author = [
     {
        id: '01',
        testimonialText: "The difference between a Designer and Developer, when it comes to design skills, is the difference between shooting a bullet and throwing it.",
        testimonialAuthor:"SCOTT HANSELMAN",

     },
     {
        id: '02',
        testimonialText:"To create anything-whether a short story or a magazine profile or a film or a sitcom–is to believe, if only momentarily, you are capable of magic.",
        testimonialAuthor:"TOM BISSEL",
     },
     {
        id:"03",
        testimonialText:"As a profession, graphic designers have been shamefully remiss or ineffective about plying their craft for social or political betterment.",
        testimonialAuthor:"STEVEN HELLER",
     }

   ]
  return (
    <>
       <div id="news" className="section">
            <div className="block content-1170 center-relative">
            <CardSection id="04" name="STORIES" className="right"/>
                <div className="section-content-holder left">
                    <div className="content-wrapper">
                        <div className="blog-holder block center-relative">

                            <article className="relative blog-item-holder center-relative">
                                <div className="num">01</div>
                                <div className="info">
                                    <div className="author vcard ">Robert Williams</div>
                                    <div className="cat-links">
                                        <ul>
                                            <li><a href="#">BRANDING</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <h3 className="entry-title">
                                    <a href="#">Eiusmod tempor incididunt ut dolore magna labore eiusmod ipsum dolor</a>
                                </h3>
                                <div className="clear"></div>
                            </article>
                            <article className="relative blog-item-holder center-relative">
                                <div className="num">02</div>
                                <div className="info">
                                    <div className="author vcard ">Jim Davis</div>
                                    <div className="cat-links">
                                        <ul>
                                            <li><a href="#">TECH</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <h3 className="entry-title">
                                    <a href="#">Incididunt ut dolore magna labore eiusmod lorem ipsum dolor sit</a>
                                </h3>
                                <div className="clear"></div>
                            </article>

                            <article className="relative blog-item-holder center-relative">
                                <div className="num">03</div>
                                <div className="info">
                                    <div className="author vcard ">Ann Peterson</div>
                                    <div className="cat-links">
                                        <ul>
                                            <li><a href="#">CRAFTING</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <h3 className="entry-title">
                                    <a href="#">Labore eiusmod lorem ipsum dolor sit amet nunc labore incididunt ut dolore</a>
                                </h3>
                                <div className="clear"></div>
                            </article>

                            <article className="relative blog-item-holder center-relative">
                                <div className="num">04</div>
                                <div className="info">
                                    <div className="author vcard ">Robert Williams</div>
                                    <div className="cat-links">
                                        <ul>
                                            <li><a href="#">CRAFTING</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <h3 className="entry-title">
                                    <a href="#">Dolor sit amet nunc labore incididunt ut dolore magna labore eiusmod</a>
                                </h3>
                                <div className="clear"></div>
                            </article>

                            <div className="latest-post-bottom-text">
                                <a href="#">GO TO BLOG</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="clear"></div>
            </div>
            <div className="block content-1170 center-relative">
                <div className="extra-content-left">
                    <div className="text1 testimonial-slider-holder slider-holder">
                        <div className="caroufredsel_wrapper">
                            <ul id="text1" className="slides testimonial p-0">
                                <li style={{width:"500px"}}>
                                    <div className="testimonial-content">
                                        <p className="testimonial-text mb-0">{author[slideBlog].testimonialText}</p>
                                        <p className="testimonial-author mb-0">{author[slideBlog].testimonialAuthor}</p>
                                    </div>
                                    <div className="clear">
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <a id="text1_next" className="carousel_text_next" onClick={() =>{
                             if((author.length -1) > slideBlog ){
                                setSlideBlog((prev) => prev +1); 

                             }else{
                                setSlideBlog(0);
                             }

                        }}></a>
                        <div className="clear"></div>
                    </div>
                </div>
            </div>
        </div>
    </>
  )
}

export default News