import React, { useState } from "react";
import PropTypes from "prop-types";
import CardSection from "../../Component/CardSection";

const Contact = (props) => {
  const [value, setValue] = useState(props.name);

  const handleChange = (event) => {
    setValue(event.target.value);
  };
  const handleSubmit = (event) => {
    event.preventDefault();
  };
  return (
    <>
      {/* Contact */}
      <div id="contact" className="section">
        <div className="block content-1170 center-relative">
          <CardSection id="07" name="Contact" className="left" />
          <div className="section-content-holder right">
            <div className="content-wrapper">
              <div className="one_half ">
                <p>
                  Consectetur adipisicing elit sed eiusmod tempor incididunt ut
                  dolore magna labore eiusmod. Lorem ipsum dolor sit amet
                  consectetur est adipisicing elit, sed do eiusmod tempor.
                </p>
                <br />
                <p>
                  <strong>
                    <span style={{ color: "#e64b77" }}>
                      Incididunt ut dolore
                    </span>
                  </strong>
                  magna labore eiusmod. Dolor sit amet consectetur est
                  adipisicing elit, sed do eiusmod.
                </p>
              </div>
              <div className="one_half last">
                <div className="contact-form">
                  <form onSubmit={handleSubmit}>
                    <p>
                      <input
                        id="name"
                        type="text"
                        name="your-name"
                        placeholder="Name"
                      />
                    </p>
                    <p>
                      <input
                        id="contact-email"
                        type="email"
                        name="your-email"
                        placeholder="Email"
                      />
                    </p>
                    <p className="mb-0">
                      <input
                        id="subject"
                        type="text"
                        name="your-subject"
                        placeholder="Subject"
                      />
                    </p>
                    <p className="mb-0">
                      <textarea
                        id="message"
                        name="your-message"
                        value={value}
                        placeholder="Message"
                        onChange={handleChange}
                      />
                    </p>
                    <p>
                      <input type="submit" value="SEND" />
                    </p>
                  </form>
                </div>
              </div>
              <div className="clear"></div>
            </div>
            <div className="full-width">
              <iframe
                src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d4274.290564544589!2d-75.2952832049782!3d40.753669641460846!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2srs!4v1444506658649"
                width="600"
                height="450"
                frameBorder="0"
                style={{ border: 0 }}
                allowFullScreen
              />
            </div>
          </div>
          <div className="clear"></div>
        </div>
      </div>
    </>
  );
};
Contact.propTypes = {
  name: PropTypes.string,
};
export default Contact;
