import React, { useEffect, useState, useRef } from "react";
import "../../Assest/css/common.css";
import "../../Assest/css/clear.css";
import "../../Assest/css/sm-clean.css";
import "../../Assest/css/style.css";
import img04 from "../../Assest/demo-images/about_img_04.jpg";
import img05 from "../../Assest/demo-images/about_img_05.jpg";
import img06 from "../../Assest/demo-images/about_img_06.jpg";
import img001 from "../../Assest/demo-images/about_img_01.jpg";
import img002 from "../../Assest/demo-images/about_img_02.jpg";
import img003 from "../../Assest/demo-images/about_img_03.jpg";
import CardSection from "../../Component/CardSection";
import InnerListing from "../../Component/InnerListing";

const About = () => {
  const [slideData, setSlideData] = useState(0);
  const addSlide = [img04, img05, img06];
  const [memberData, setMemberData] = useState(0);

  const members = [
    {
      id: 1,
      memberName: "Robert Williams",
      memberPosition: "LEAD DESIGNER",
      img: img001,
    },
    {
      id: 2,
      memberName: "John Doe",
      memberPosition: "SEO MASTER",
      img: img002,
    },
    {
      id: 3,
      memberName: "John Doe",
      memberPosition: "PSD GURU",
      img: img003,
    },
  ];

  return (
    <>
      {/* <!-- About --> */}
      <div id="about" className="section">
        <div className="block content-1170 center-relative">
          <CardSection id="03" name="CRAFTERS" className="left" />
          <div className="section-content-holder right">
            <InnerListing
              title={`<p>Polor sit amet consectetur adipisicing elit sed eiusmod tempor incididunt ut dolore magna labore eiusmod. Lorem ipsum <strong>dolor sit amet</strong> consectetur est adipisicing elit, sed do eiusmod tempor</p>`}
              name="About"
              className="text-pink"
            />
            <div className="full-width ">
              <div className="image-slider-wrapper relative img aboutImage">
                <a
                  id="aboutImage_next"
                  className="image_slider_next"
                  onClick={() => {
                    if (addSlide.length - 1 > slideData) {
                      setSlideData((prev) => prev + 1);
                    } else {
                      setSlideData(0);
                    }
                  }}
                ></a>
                <div className="caroufredsel_wrapper">
                  <ul id="aboutImage" className="image-slider slides p-0">
                    <li>
                      <img src={addSlide[slideData]} alt="sliders-img" />
                    </li>
                  </ul>
                </div>
                <div className="clear"></div>
              </div>
              <div className="image-slider-wrapper relative team team1">
                <a
                  id="team1_next"
                  className="image_slider_next"
                  onClick={() => {
                    if (members.length - 1 > memberData) {
                      setMemberData((prev) => prev + 1);
                    } else {
                      setMemberData(0);
                    }
                  }}
                ></a>
                <div className="caroufredsel_wrapper">
                  <ul id="team1" className="image-slider slides">
                    <li style={{ width: "800px" }}>
                      <div className="member-content-holder">
                        <h4 className="member-name">
                          {members[memberData].memberName}
                        </h4>
                        <p className="member-position">
                          {members[memberData].memberPosition}
                        </p>
                        <div className="member-content">
                          Eiusmod tempor incididunt ut dolore magna labore
                          eiusmod. Lorem ipsum dolor sit amet consectetur est
                          lorem adipisicing elit, sed do eiusmod tempor polor
                          sit amet consectetur.
                          <br />
                        </div>
                      </div>
                      <div className="member-image-holder">
                        <img src={members[memberData].img} alt="img001" />
                      </div>
                      <div className="clear"></div>
                    </li>
                  </ul>
                </div>
                <div className="clear"></div>
              </div>
            </div>
          </div>
          <div className="clear"></div>
        </div>
      </div>
    </>
  );
};

export default About;
