import React, { useEffect, useState } from "react";
import "../../Assest/css/style.css";
import "../../Assest/css/clear.css";
import "../../Assest/css/common.css";
import portfolio from "../../Assest/demo-images/portfolio_item_01.jpg";
import iconPost from "../../Assest/images/icon_post.svg";
import portfolio01 from "../../Assest/demo-images/portfolio_item_02.jpg";
import iconPost01 from "../../Assest/images/icon_post.svg";
import portfolio02 from "../../Assest/demo-images/portfolio_item_08.jpg";
import portfolio03 from "../../Assest/demo-images/portfolio_item_05.jpg";
import iconInfinity from "../../Assest/images/icon_infinity.svg";
import { Link } from "react-router-dom";
import CardSection from "../../Component/CardSection";

const Portfolio = () => {

  return (
    <>
      {/* Portfolio  */}

      <div id="portfolio" className="section">
        <div className="block content-1170 center-relative">
        <CardSection id="02" name="Portfolio" className="right"/>
          <div className="section-content-holder portfolio-holder left">
            <div
              className="grid"
              id="portfolio-grid"
              style={{ position: "relative", height: " 1600px" }}
            >
              <div className="grid-sizer"></div>
              <div
                className="grid-item element-item p_one"
                style={{ position: "absolute", left: "0px", top: "0px" }}
              >
                <Link to="portfolio_1">
                  <img src={portfolio} alt="portfolioItem"/>
                  <div className="portfolio-text-holder">
                    <div className="portfolio-text-wrapper" style={{ marginTop: "100px" }}>
                      <p className="portfolio-type">
                        <img src={iconPost} alt="iconPost" />
                      </p>
                      <p className="portfolio-text">PSD MOCKUP</p>
                      <p className="portfolio-sec-text">Smart Watch</p>
                    </div>
                  </div>
                </Link>
              </div>
              <div
                className="grid-item element-item p_one_half"
                style={{ position: "absolute", left: "0px", top: "400px" }}
              >
                <Link to="#">
                  <img src={portfolio01} alt="" />
                  <div className="portfolio-text-holder">
                    <div className="portfolio-text-wrapper" style={{ marginTop: "100px" }}>
                      <p className="portfolio-type">
                        <img src={iconPost01} alt="icon_post01" />
                      </p>
                      <p className="portfolio-text">PSD MOCKUP</p>
                      <p className="portfolio-sec-text">Smart Watch</p>
                    </div>
                  </div>
                </Link>
              </div>
              <div
                className="grid-item element-item p_one_half"
                style={{ position: "absolute", left: "400px", top: "400px" }}
              >
                <a
                  data-rel="prettyPhoto[gallery1]"
                  href="demo-images/portfolio_item_08.jpg"
                >
                  <img src={portfolio02} alt="portfolio_item01" />
                  <div className="portfolio-text-holder">
                    <div className="portfolio-text-wrapper" style={{ marginTop: "100px" }}>
                      <p className="portfolio-type">
                        <img src={iconPost01} alt="icon_post02" />
                      </p>
                      <p className="portfolio-text">PSD MOCKUP</p>
                      <p className="portfolio-sec-text">Smart Watch</p>
                    </div>
                  </div>
                </a>
              </div>
              <div
                className="grid-item element-item p_one"
                style={{ position: "absolute", left: "0px", top: "800px" }}
              >
                <a
                  data-rel="prettyPhoto[gallery1]"
                  href="demo-images/portfolio_item_05.jpg"
                >
                  <img src={portfolio03} alt="portfolio_item_05" />
                  <div className="portfolio-text-holder">
                    <div className="portfolio-text-wrapper" style={{marginTop: "250.5px"}}>
                      <p className="portfolio-type">
                        <img src={iconPost} alt="icon_post04" />
                      </p>
                      <p className="portfolio-text">PSD MOCKUP</p>
                      <p className="portfolio-sec-text">Smart Watch</p>
                    </div>
                  </div>
                </a>
              </div>
            </div>
            <div className="clear"></div>
            <div className="block portfolio-load-more-holder">
              <a target="_self" className="more-posts">
                LOAD MORE
              </a>
              <img src={iconInfinity} alt="Load more" />
            </div>
          </div>
          <div className="clear"></div>
        </div>
        <div className="clear"></div>
      </div>
    </>
  );
};

export default Portfolio;
