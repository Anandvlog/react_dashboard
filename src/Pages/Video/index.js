import React from "react";
import CardSection from "../../Component/CardSection";
import videoImg from "../../Assest/demo-images/video_img.jpg";
import videoIcon from "../../Assest/images/icon_video.svg";
import CustomSlider from "../../Component/CustomSlider";
import InnerListing from "../../Component/InnerListing";

const Video = () => {
  return (
    <>
      {/* Video */}

      <div id="video" className="section">
        <div className="block content-1170 center-relative">
          <CardSection id="05" name="OFFER" className="left" />
          <div className="section-content-holder right">
          <InnerListing name="Video" className="listingColor01 text-yellow" title={`<p>Polor sit amet consectetur adipisicing elit sed eiusmod tempor incididunt ut dolore magna labore eiusmod. Lorem ipsum <strong>dolor sit amet</strong> consectetur est adipisicing elit, sed do eiusmod tempor</p>`} />
           <div className="full-width ">
              <a
                className="video-popup-holder"
                href="https://vimeo.com/157276599"
              >
                <img className="thumb" src={videoImg} alt="videoImg" />
                <img
                  className="popup-play"
                  src={videoIcon}
                  alt="Play"
                />
              </a>
            </div>
          </div>
          <div className="clear"></div>
        </div>
        <div className="extra-content-full-width">
          <div className="fwslider1 fw-image-slider-holder list_carousel relative">
            <div className="caroufredsel_wrapper">
            <ul className="fw-image-slider p-0 m-0">
               <CustomSlider />
            </ul>
            </div>
            <div className="clear"></div>
            <div
              id="fwslider1_fw_image_slide_pager"
              className="fw_carousel_pagination"
            ></div>
          </div>
          <a id="fwslider1_fw_next" className="carousel_fw_next" href="#"></a>
          <div className="clear"></div>
        </div>
      </div>
    </>
  );
};

export default Video;
