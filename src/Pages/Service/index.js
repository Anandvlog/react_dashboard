import React, { useCallback, useState } from "react";
import "../../Assest/css/style.css";
import "../../Assest/css/clear.css";
import "../../Assest/css/common.css";
import icon1 from "../../Assest/demo-images/icon_01.png";
import icon2 from "../../Assest/demo-images/icon_02.png";
import icon3 from "../../Assest/demo-images/icon_03.png";
import icon4 from "../../Assest/demo-images/icon_04.png";
import CardSection from "../../Component/CardSection";

const Service = () => {
    
    const [data,setData] = useState(0);
    const useData = useCallback(() =>{
        setData(!data);
     },[data])

    const service_Slide =[{
       id:1,
       img: icon1,
       serviceTitle:'BRANDING',
       serviceContent:'Donecos arem ipsum sit amet consectetur adipisicing elit sed eiusmod tempor incididunt ut donecos dolore ipsum temporest',
      
    },{
      id:2,
      img: icon2,
      serviceTitle:'MOBILE APPS',
      serviceContent:"Disum lorem sit amet consectetur adipisicing elit sed eiusmod tempor incididunt ut donecos dolore ipsum temporest."
    },{
      id:3,
      img: icon3,
      serviceTitle:'WEB',
      serviceContent:"Polor sit amet consectetur adipisicing elit sed eiusmod tempor incididunt ut dolore magna labore eiusmod."
  },{
     id:4,
     img: icon4,
     serviceTitle:'GRAPHIC',
     serviceContent:"Cadipisicing elit sed eiusmod tempor incididunt ut labore lorem ipsum dolor sit amet consectetur lorem ipsum dolor sit amet.",
  },{
    id:5,
    img: icon3,
    serviceTitle:"SERVICES",
    serviceContent:"Donecos arem ipsum sit amet consectetur adipisicing elit sed eiusmod tempor incididunt ut donecos dolore ipsum temporest.",
  },{
    id:6,
    img: icon4,
    serviceTitle:'PSD',
    serviceContent:"Disum lorem sit amet consectetur adipisicing elit sed eiusmod tempor incididunt ut donecos dolore ipsum temporest.",
  },{
    id:7,
    img: icon2,
    serviceTitle:'HTML',
    serviceContent:"Polor sit amet consectetur adipisicing elit sed eiusmod tempor incididunt ut dolore magna labore  eiusmod.",
  },{
    id:8,
    img: icon1,
    serviceTitle:'PHP',
    serviceContent:"Cadipisicing elit sed eiusmod tempor incididunt ut labore lorem ipsum dolor sit amet consectetur lorem ipsum dolor sit amet.",
  }]

  return (
    <>
      {/* Service */}

      <div id="services" className="section">
        <div className="block content-1170 center-relative">
        <CardSection id="01" name="Services" className="left"/>
          <div className="section-content-holder right">
            <div className="content-wrapper">
              <div className="image-slider-wrapper relative service slider1">
                <a
                  id="slider1_next"
                  className="image_slider_next"
                   onClick={useData}
                ></a>
                <ul id="slider1" className="image-slider slides">
                <li style={{ width: "680px" }} >
                {service_Slide.slice(...(data ? [4] : [0,4])).map((sTitle,index) =>{
                   return (
                   <React.Fragment key={index}>
                    <div className="service-holder ">
                      <img src={sTitle.img} alt="icon" />
                      <div className="service-content-holder">
                        <div className="service-title">{sTitle.serviceTitle}</div>
                        <div className="service-content">
                          {sTitle.serviceContent}
                          <br />
                        </div>
                      </div>
                    </div>
                  </React.Fragment>
                   );
                })}
                   </li>
                </ul>
                <div className="clear"></div>
              </div>
            </div>
          </div>
          <div className="clear"></div>
        </div>
      </div>
    </>
  );
};
export default Service;
