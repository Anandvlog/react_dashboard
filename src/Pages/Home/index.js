import React from 'react'
import '../../Assest/css/clear.css';
import '../../Assest/css/common.css';
import '../../Assest/css/sm-clean.css';
import '../../Assest/css/style.css';
import logo from '../../Assest/demo-images/logo.png';

const Home = () => {
  return (
   <>
   {/* Home Component */}
     <div id="home" className="section intro-page">
            <div className="block content-1170 center-relative center-text">
                <img className="top-logo" src={logo} alt="Boxus" />
                <br />
                <h1 className="big-title">We Craft Awesome Web And <span>Graphic Design Solutions</span></h1>
                <p className="title-desc">Support bright students today for a better tomorrow</p>
            </div>
        </div>
   </>
  )
}
export default Home