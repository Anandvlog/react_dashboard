import React, { useEffect, useState,useRef } from 'react';
import Home from './Pages/Home';
import 'bootstrap/dist/css/bootstrap.min.css';
import Service from './Pages/Service';
import Portfolio from './Pages/Portfolio';
import Header from './Component/Header';
import Footers from './Component/Footers';
import About from './Pages/About';
import News from './Pages/News';
import PageLoader from './Component/PageLoader';
import Video from './Pages/Video';
import Skill from './Pages/Skill';
import Contact from './Pages/Contact';
import Portfolio_1 from './Component/Portfolio_1';



function App() {

  const [isLoading, setIsLoading ] = useState(true);
  useEffect(() => {
    setTimeout(() => setIsLoading(false), 1000)
  }, []);  
  return (
    <div className="app">
     {isLoading && <PageLoader />}
     <Header />
        {/* <Routes> 
         <Route path="portfolio"  element={<Portfolio/>} >
          <Route path=":portfolio-1" element={<Portfolio_1 />} /> 
         </Route>
       </Routes> */}

      {/* Preloader Gif */}
        <Home />
        <Service />
        <Portfolio />
        <About />
        <News />
        <Video />
        <Skill />
        <Contact />
        <Footers />
      
      </div>
  );
}

export default App;
